'use strict';

/****************************************************************************
* Initial setup
****************************************************************************/

// var configuration = {
//   'iceServers': [{
//     'url': 'stun:stun.l.google.com:19302'
//   }]
// };
// {'url':'stun:stun.services.mozilla.com'}

var configuration = null;

var video = document.querySelector('video');
var roomData = document.querySelector('textarea#roomData');
var sendBtn = document.getElementById('send');
var recvBtn = document.getElementById('receive');
var stopBtn = document.getElementById('stop');
sendBtn.disabled = false;
recvBtn.disabled = false;
stopBtn.disabled = true;

var token;
var room;

var sender;
var localStream;
var receiver;
var remoteStream;

// Attach event handlers
sendBtn.addEventListener('click', requestSend);
recvBtn.addEventListener('click', requestReceive);
stopBtn.addEventListener('click', terminateSession);
window.addEventListener('beforeunload', terminateSession, false);

/****************************************************************************
* Signaling server
****************************************************************************/

// Connect to the signaling server
var socket = io.connect('http://192.168.1.9:8080');

/**
* Signaling with server
*/

socket.on('log', function(array) {
  console.log.apply(console, array);
});

socket.on('ems message', function(message) {
  console.log('Client received message:', message);
  signalingMessageCallback(message);
});

/**
* Signaling with sender
*
*   Sender			      Server 		             EMS
*   ------                ------                     ---
*     |  'create sender'    ||                        |
* 	  |-------------------->||  'ems create sender'   |
* 	  |                     ||----------------------->|
*     |                     ||                        |
*     |                     ||        'offer'         |
*     |      'offer'        ||<-----------------------|
*     |<--------------------||                        |
*     |                     ||                        |
*     |      'answer'       ||                        |
*     |-------------------->||       'answer'         |
*     |                     ||----------------------->|
*/

function requestSend() {
  console.log('Client requests to send');
  receiver = false;
  sender = true;
  remoteStream = undefined;
  room = roomData.value;
  window.location.hash = room;
  grabWebCamVideo()
  sendBtn.disabled = true;
  recvBtn.disabled = true;
  stopBtn.disabled = false;
}

socket.on('sender created', function(newToken, newRoom) {
  token = newToken;
  room = newRoom;
  window.location.hash = room;
  console.log('Created sender with token: ', token);
});

/**
* Signaling with receiver
*
*  Receiver 		      Server 		             EMS
*  --------               ------                     ---
*     |  'create receiver'  ||                        |
* 	  |-------------------->|| 'ems create receiver'  |
* 	  |                     ||----------------------->|
*     |                     ||                        |
*     |                     ||        'offer'         |
*     |      'offer'        ||<-----------------------|
*     |<--------------------||                        |
*     |                     ||                        |
*     |      'answer'       ||                        |
*     |-------------------->||       'answer'         |
*     |                     ||----------------------->|
*/

function requestReceive() {
  console.log('Client requests to receive');
  sender = false;
  receiver = true;
  localStream = undefined;
  room = roomData.value;
  sendBtn.disabled = true;
  recvBtn.disabled = true;
  stopBtn.disabled = false;
  socket.emit('create receiver', room);
}

socket.on('receiver created', function(newToken) {
  token = newToken
  console.log('Created sender with token: ', token);
});

/**
* Send message to signaling server
*/
function sendMessage(message) {
  message = {
	content: message,
	token: socket.io.engine.id
  }
  console.log('Client token: ', socket.io.engine.id);
  console.log('Client sending message: ', message);
  socket.emit('message', message);
  console.log('Client message sent: ', message);
}

/****************************************************************************
* User media (webcam)
****************************************************************************/

function grabWebCamVideo() {
  console.log('Getting user media (video) ...');
  navigator.mediaDevices.getUserMedia({
    audio: true,
    video: true
  })
  .then(gotStream);
}

function gotStream(stream) {
  console.log('Adding local stream.');
  video.src = window.URL.createObjectURL(stream);
  localStream = stream;
  sendMessage('got user media: ');
  socket.emit('create sender', room);
}

/****************************************************************************
* WebRTC peer connection and data channel
****************************************************************************/

var peerConn;
var dataChannel;

function signalingMessageCallback(message) {
  if (message.type === 'offer') {
    console.log('Got offer.');
    createPeerConnection(configuration, token);
    console.log('Creating an answer');
    peerConn.setRemoteDescription(new RTCSessionDescription(message), 
							      function() {}, logError);
    peerConn.createAnswer(onLocalSessionCreated, logError);

  } else if (message.type === 'candidate') {
    peerConn.addIceCandidate(new RTCIceCandidate({
      candidate: message.candidate
    }));

  } else if (message === 'bye') {
// TODO: cleanup RTC connection?
}
}

function createPeerConnection(config, token) {
  peerConn = new RTCPeerConnection(config);

  // send any ice candidates to the other peer
  peerConn.onicecandidate = function(event) {
    console.log('icecandidate event:', event);
    if (event.candidate) {
      sendMessage({
        type: 'candidate',
        label: event.candidate.sdpMLineIndex,
        id: event.candidate.sdpMid,
        candidate: event.candidate.candidate
      });
    } else {
      console.log('End of candidates.');
    }
  };
  
  if (sender) {
    console.log('Creating Stream Channel');
    peerConn.addStream(localStream)

  } else if (receiver) {
	peerConn.onaddstream = handleNewStream;
  };
}

function handleNewStream(event) {
  console.log('Remote stream added.');
  video.src = window.URL.createObjectURL(event.stream);
  remoteStream = event.stream;
};

function onLocalSessionCreated(desc) {
  console.log('local session created:', desc);
  peerConn.setLocalDescription(desc, function() {
    console.log('sending local desc:', peerConn.localDescription);
    sendMessage(peerConn.localDescription);
  }, logError);
}

function show() {
  Array.prototype.forEach.call(arguments, function(elem) {
    elem.style.display = null;
  });
}

function hide() {
  Array.prototype.forEach.call(arguments, function(elem) {
    elem.style.display = 'none';
  });
}

function randomToken() {
  return Math.floor((1 + Math.random()) * 1e16).toString(16).substring(1);
}

function logError(err) {
  console.log(err.toString(), err);
}

function terminateSession() {
  socket.emit('bye', token);
  sendBtn.disabled = false;
  recvBtn.disabled = false;
  stopBtn.disabled = true;
  peerConn = undefined;
  remoteStream = undefined;
  localStream = undefined;
  token = undefined;
  sender = false;
  receiver = false;
  video.src = "";
}
  
