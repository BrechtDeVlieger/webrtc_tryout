'use strict';

/****************************************************************************
* Initial setup
****************************************************************************/

// var configuration = {
//   'iceServers': [{
//     'url': 'stun:stun.l.google.com:19302'
//   }]
// };
// {'url':'stun:stun.services.mozilla.com'}

var configuration = null;
var offerConstraints = {
  offerToReceiveAudio: 1,
  offerToReceiveVideo: 1
}

var roomURL = document.getElementById('url');
var connections = {}
var availableScreens = []
var remoteStream;
availableScreens[0] = document.querySelector('#sender1')
availableScreens[1] = document.querySelector('#sender2')
availableScreens[2] = document.querySelector('#sender3')

// Attach event handlers

/****************************************************************************
* Signaling server
****************************************************************************/

// Connect to the signaling server
var socket = io.connect('http://192.168.1.9:8080');

/**
* Signaling with server
*/
socket.on('log', function(array) {
  console.log.apply(console, array);
});

socket.on('message', function(message) {
  console.log('EMS received message:', message);
  handleMessage(message);
});

// Create EMS server
console.log('Create EMS')
socket.emit('EMS connect');

/**
* Signaling with sender
*
*   Sender			      Server 		             EMS
*   ------                ------                     ---
*     |  'create sender'    ||                        |
* 	  |-------------------->||  'ems create sender'   |
* 	  |                     ||----------------------->|
*     |                     ||                        |
*     |                     ||        'offer'         |
*     |      'offer'        ||<-----------------------|
*     |<--------------------||                        |
*     |                     ||                        |
*     |      'answer'       ||                        |
*     |-------------------->||       'answer'         |
*     |                     ||----------------------->|
*/
socket.on('ems create sender', function(token, room) {
  room = uniqueRoom(room);
  createSender(token, room);
});

/**
* Signaling with receiver
*
*  Receiver 		      Server 		             EMS
*  --------               ------                     ---
*     |  'create receiver'  ||                        |
* 	  |-------------------->|| 'ems create receiver'  |
* 	  |                     ||----------------------->|
*     |                     ||                        |
*     |                     ||        'offer'         |
*     |      'offer'        ||<-----------------------|
*     |<--------------------||                        |
*     |                     ||                        |
*     |      'answer'       ||                        |
*     |-------------------->||       'answer'         |
*     |                     ||----------------------->|
*/
socket.on('ems create receiver', function(token, room) {
  createReceiver(token, room);
});

socket.on('bye', function(token){
  cleanUp(token);
});


/**
* Send message to signaling server
*/
function sendMessage(message, token) {
  message = {
	content: message,
	token: token
  }
  console.log('EMS sending message: ', message);
  socket.emit('ems message', message);
}

/****************************************************************************
* Handle messages
****************************************************************************/


function handleMessage(message) {
  var content = message.content;
  console.log('Received content:', content);
  if (content.type === 'answer') {
    console.log('Got answer:', content);
	var peerConn = connections[message.token].peerConn
    peerConn.setRemoteDescription(new RTCSessionDescription(content), 
								  function() {}, logError);

  } else if (content.type === 'candidate') {
	var peerConn = connections[message.token].peerConn
    peerConn.addIceCandidate(new RTCIceCandidate({
      candidate: content.candidate
    }));
  }
}

/****************************************************************************
* Create PeerConnection
****************************************************************************/

function createSender(token, room) {
  console.log('Created sender', token, 'for room', room);
  createPeerConnection(configuration, token);
  connections[token].room = room;
  connections[token].type = 'sender';
  var peerConn = connections[token].peerConn;

  peerConn.onaddstream = event => handleNewStream(event, token);
  peerConn.createOffer(desc => setLocalDescAndOffer(desc, token), 
		  			   logError, offerConstraints);
  socket.emit('ems sender created', token, room);
}

function createReceiver(token, room) {
  console.log('Created receiver:', token, 'in room:', room);
  createPeerConnection(configuration, token);
  connections[token].type = 'receiver';
  connections[token].room = room;
  var peerConn = connections[token].peerConn;
  var senderToken = findSender(room);
  if (senderToken) {
    var stream = connections[senderToken].stream;
  }
  connections[token].stream = stream;
  peerConn.addStream(stream);
  peerConn.createOffer(desc => setLocalDescAndOffer(desc, token), 
		  			   logError);
  socket.emit('ems receiver created', token);
}

function createPeerConnection(config, token) {
  connections[token] = {};
  var peerConn = new RTCPeerConnection(config);

  // send any ice candidates to the other peer
  peerConn.onicecandidate = event => handleNewCandidate(event, token);
  peerConn.oniceconnectionstatechange = event => handleSenderIceChange(event, token);
  
  connections[token].peerConn = peerConn;
}

function handleNewCandidate(event, token) {
  console.log('icecandidate event:', event);
  if (event.candidate) {
	var message = {
      type: 'candidate',
      label: event.candidate.sdpMLineIndex,
      id: event.candidate.sdpMid,
      candidate: event.candidate.candidate
    };
    sendMessage(message, token);
  } else {
    console.log('End of candidates.');
  }
};

function handleNewStream(event, token) {
  console.log('Remote stream added.');
  // Take the first available video element out of the list
  var video = availableScreens.splice(0, 1)[0];
  console.log('video: ', video);
  video.src = window.URL.createObjectURL(event.stream);
  remoteStream = event.stream;
  connections[token].stream = event.stream;
  connections[token].video = video;
};

function handleSenderIceChange(event, token){
  var peerConn = connections[token].peerConn;
  if (peerConn.iceConnectionState === 'disconnected' ||
      peerConn.iceConnectionState === 'failed' ||
	  peerConn.iceConnectionState === 'closed') {
	cleanUp(token);
  }
}

/****************************************************************************
* Answer offer of sender
****************************************************************************/


function setLocalDescAndOffer(desc, token) {
  var peerConn = connections[token].peerConn;
  console.log('local session created:', desc);
  peerConn.setLocalDescription(desc, function() {
    console.log('sending local desc:', 
			    peerConn.localDescription);
    sendMessage(peerConn.localDescription, token);
  }, logError);
}

/****************************************************************************
* Aux functions, mostly UI-related
****************************************************************************/

function show() {
  Array.prototype.forEach.call(arguments, function(elem) {
    elem.style.display = null;
  });
}

function hide() {
  Array.prototype.forEach.call(arguments, function(elem) {
    elem.style.display = 'none';
  });
}

function randomToken() {
  return Math.floor((1 + Math.random()) * 1e16).toString(16).substring(1);
}

function logError(err) {
  console.log(err.toString(), err);
}

function findSender(room) {
  for (var token in connections) {
	if (connections[token].room === room &&
		connections[token].type === 'sender') {
	  return token;
	}
  }
}

function uniqueRoom(room) {
  if (findSender(room)) {
	var index = 1;
	var newRoom = room + index;
	while (findSender(newRoom)) {
	  index += 1;
	  newRoom = room + index;
	};
	return newRoom;
  } else {
    return room;
  }
}
    
function cleanUp(token) {
  console.log('Sender ', token, ' disconnected');
  var video = connections[token].video;
  video.src = '';
  availableScreens.push(video);
  delete connections[token];
}
