'use strict';

var os = require('os');
var nodeStatic = require('node-static');
var http = require('http');
var socketIO = require('socket.io');
var ems;

var fileServer = new(nodeStatic.Server)();
var app = http.createServer(function(req, res) {
  fileServer.serve(req, res);
}).listen(8080);

var io = socketIO.listen(app);
io.sockets.on('connection', function(socket) {

  // convenience function to log server messages on the client
  function log() {
    var array = ['Message from server:'];
    array.push.apply(array, arguments);
    socket.emit('log', array);
  }

  // convenience function to log server messages on the EMS client
  function emslog() {
    var array = ['Message from server:'];
    array.push.apply(array, arguments);
    io.sockets.connected[ems].emit('log', array);
  }

  socket.on('ems message', function(message) {
    log('EMS said: ', message);
    // for a real app, would be room-only (not broadcast)
	var token = message.token;
	var content = message.content;
    io.sockets.connected[token].emit('ems message', content);
  });

  socket.on('message', function(message) {
    log('Client said: ', message);
    // for a real app, would be room-only (not broadcast)
    socket.broadcast.emit('message', message);
  });

  /************************
  * Initialization of EMS *
  ************************/
  socket.on('EMS connect', function(room) {
	// use slice to store the value instead of the reference
	ems = socket.id.slice(0); 
	log('EMS connected: ' + ems + '. Room created: ' + room)
  });

  /***********************************
  * Signaling between sender and EMS *
  ***********************************/
  socket.on('create sender', function(room) {
	var token = socket.id;
	log('Create ems endpoint for sender')
	log('ems id:', ems)
	emslog('Sender trying to connect', room)
	io.sockets.connected[ems].emit('ems create sender', token, room)
  });

  socket.on('ems sender created', function(token, room) {
	log('Sender created with token ', token, 'and room', room)
	io.sockets.connected[token].emit('sender created', token, room)
  });

  /*************************************
  * Signaling between receiver and EMS *
  *************************************/
  socket.on('create receiver', function(room) {
	var token = socket.id
	log('Create ems endpoint for receiver')
	log('ems id:', ems)
	emslog('Receiver trying to connect')
	io.sockets.connected[ems].emit('ems create receiver', token, room)
  });

  socket.on('ems receiver created', function(token) {
	log('Receiver created with token ' + token)
	io.sockets.connected[token].emit('receiver created', token)
  });

  socket.on('bye', function(token){
    console.log('received bye');
	io.sockets.connected[ems].emit('bye', token)
  });

});

function randomToken() {
  return Math.floor((1 + Math.random()) * 1e16).toString(16).substring(1);
}
